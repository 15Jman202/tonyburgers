//
//  BurgerSubmitTableViewController.swift
//  Tonyburgers
//
//  Created by Justin Carver on 11/16/16.
//  Copyright © 2016 Justin Carver. All rights reserved.
//

import UIKit

class BurgerSubmitTableViewController: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userNameTextField.delegate = self
        userPhoneNumberTextField.delegate = self
        userFavLocationTextField.delegate = self
        userEmailTextField.delegate = self
        nameYourBurgerTextField.delegate = self
        let deviceType = UIDevice().type
        if deviceType == Model.iPhone6S || deviceType == Model.iPhone6plus || deviceType == Model.iPhone6Splus || deviceType == Model.iPhone7 || deviceType == Model.iPhone7plus {
            upArrow.title = ""
            upArrow.image = nil
            upArrow.isEnabled = false
            downArrow.title = ""
            downArrow.image = nil
            downArrow.isEnabled = false
        }
        let imageview = UIImageView(image: #imageLiteral(resourceName: "Logo"))
        imageview.contentMode = .scaleAspectFit
        imageview.frame = CGRect(x: 0, y: 0, width: 50, height: 45)
        navigationItem.titleView = imageview
        NotificationCenter.default.addObserver(self, selector: #selector(updateSelectedIndexPaths(notification:)), name: NSNotification.Name(rawValue: "selectedIndexPathsUpdated"), object: nil)
    }
    
    var image: UIImage?
    
    var selectedIndexPaths: [IndexPath] = []
    
    func updateSelectedIndexPaths(notification: Notification) {
        let indexPaths = notification.object as? [IndexPath]
        selectedIndexPaths = indexPaths!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    //==========================================================================
    //  MARK: - Segue
    //==========================================================================
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "toBurgerToppings" else { return }
        let desinationVC = segue.destination as? BurgerToppingsTableViewController
        desinationVC?.selectedIndexPaths = selectedIndexPaths
    }
    
    //=======================================================================
    //  MARK: - Outlets
    //==========================================================================
    
    @IBOutlet var upArrow: UIBarButtonItem!
    @IBOutlet var downArrow: UIBarButtonItem!
    
    @IBOutlet var userNameTextField: UITextField! {
        didSet {
            userNameTextField.layer.borderWidth = 0.5
            userNameTextField.layer.cornerRadius = 5
            userNameTextField.layer.masksToBounds = true
        }
    }
    @IBOutlet var userEmailTextField: UITextField! {
        didSet {
            userEmailTextField.layer.borderWidth = 0.5
            userEmailTextField.layer.cornerRadius = 5
            userEmailTextField.layer.masksToBounds = true
        }
    }
    @IBOutlet var userPhoneNumberTextField: UITextField! {
        didSet {
            userPhoneNumberTextField.layer.borderWidth = 0.5
            userPhoneNumberTextField.layer.cornerRadius = 5
            userPhoneNumberTextField.layer.masksToBounds = true
        }
    }
    @IBOutlet var userFavLocationTextField: UITextField! {
        didSet {
            userFavLocationTextField.layer.borderWidth = 0.5
            userFavLocationTextField.layer.cornerRadius = 5
            userFavLocationTextField.layer.masksToBounds = true
        }
    }
    @IBOutlet var nameYourBurgerTextField: UITextField! {
        didSet {
            nameYourBurgerTextField.layer.borderWidth = 0.5
            nameYourBurgerTextField.layer.cornerRadius = 5
            nameYourBurgerTextField.layer.masksToBounds = true
        }
    }
    @IBOutlet var submitButton: UIButton! {
        didSet {
            submitButton.layer.borderWidth = 0.5
        }
    }
    @IBOutlet var tellUsAboutYourBurgerButton: UIButton! {
        didSet {
            tellUsAboutYourBurgerButton.layer.borderWidth = 0.5
        }
    }
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var uploadButton: UIButton! {
        didSet {
            uploadButton.layer.borderWidth = 0.5
            uploadButton.layer.cornerRadius = 15
            uploadButton.layer.masksToBounds = true
        }
    }
    
    //==========================================================================
    //  MARK: - Text field functions
    //==========================================================================

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return false }
        if textField == userPhoneNumberTextField {
            if string == "" {
                return true
            } else {
                let numberSet = CharacterSet(charactersIn: "1234567890")
                if text.characters.count < 10 {
                    return !(string.rangeOfCharacter(from: numberSet) == nil)
                } else {
                    return false
                }
            }
        } else if textField == userEmailTextField {
            return !(string.contains(" "))
        } else {
            return true
        }
    }
    
    func resignFirstResponders() {
        userNameTextField.resignFirstResponder()
        userEmailTextField.resignFirstResponder()
        userFavLocationTextField.resignFirstResponder()
        userPhoneNumberTextField.resignFirstResponder()
        nameYourBurgerTextField.resignFirstResponder()
    }
    
    //==========================================================================
    //  MARK: - Actions
    //==========================================================================
    
    @IBAction func upArrowTapped(_ sender: UIBarButtonItem) {
        guard let firstCell = self.tableView.visibleCells.first, let firstCellIndex = self.tableView.indexPath(for: firstCell) else { return }
        self.tableView.scrollToRow(at: firstCellIndex, at: .top, animated: true)
    }
    
    @IBAction func downArrowTapped(_ sender: Any) {
        guard let lastCell = self.tableView.visibleCells.last, let lastCellIndex = self.tableView.indexPath(for: lastCell) else { return }
        self.tableView.scrollToRow(at: lastCellIndex, at: .middle, animated: true)
    }
    
    @IBAction func tellUsAboutYourBurgerTapped() {
        self.resignFirstResponders()
    }
    
    @IBAction func resignTapped(_ sender: UITapGestureRecognizer) {
        resignFirstResponders()
    }
    
    @IBAction func resignScrolled(_ sender: Any) {
        resignFirstResponders()
    }
    
    @IBAction func submitButtonTapped() {
        guard let burgerName = nameYourBurgerTextField.text,
            let userName = userNameTextField.text,
            let userEmail = userEmailTextField.text,
            let userPhone = userPhoneNumberTextField.text,
            let userFavLocation = userFavLocationTextField.text
            else {
                self.presentAlert(and: "Sorry something went wrong, please try again later.")
                return
        }
        
        guard BurgerController.canSubmitAgain == true else { presentAlert(and: "You have submitted a burger within the last 30 days. Please try again later"); return }
    
        guard self.image != nil else { presentAlert(and: "There was a problem when submitting your photo please try again."); return }
        guard nameYourBurgerTextField.text != "" else { presentAlert(and: "You must give your burger a name."); return }
        guard BurgerController.burger.size != [] else { presentAlert(and: "You must select a size for your burger."); return }
        guard userName.characters.count >= 2 else { presentAlert(and: "You must enter your full name."); return }
        guard userEmail.characters.contains("@") && userEmail.contains(".") else { presentAlert(and: "You must enter a valid email."); return }
        guard userPhone.characters.count == 10 else { presentAlert(and: "You must enter a valid phone number."); return }
        guard userFavLocation.characters.count != 0 else { presentAlert(and: "You must enter your favorite tonyburgers location."); return }

        self.resignFirstResponders()
        submitButton.setTitle("Please Wait...", for: .normal)
        tellUsAboutYourBurgerButton.isEnabled = false
        self.tableView.reloadData()
        BurgerController.burger.submitDate = Date()
        BurgerController.burger.userName = userName
        BurgerController.burger.burgerName = burgerName
        BurgerController.burger.userEmail = userEmail
        BurgerController.burger.userPhone = userPhone
        BurgerController.burger.userFavLocation = userFavLocation
        BurgerController.putBurgerInformation(burgerInfo: BurgerController.burger) { (success) in
            if success == false {
                self.presentAlert(and: "There is a problem with the connection to the server. Please check your internet connection and try again.")
                print("error")
                return
            } else {
                guard BurgerController.burger.image != nil else {
                    DispatchQueue.main.async {
                        self.resetBurgerInfo()
                        self.presentSuccessfulSubmitAlert()
                        self.tableView.reloadData()
                        print("Yay")
                    }
                    return
                }
                
                FirebaseController.shared.saveBurger(image: BurgerController.burger.image!, with: BurgerController.uuid) { (success) in
                    if !(success) {
                        self.presentAlert(and: "Failed to put image. Please Try Again")
                    }
                }
                
                DispatchQueue.main.async {
                    self.resetBurgerInfo()
                    self.presentSuccessfulSubmitAlert()
                    UserDefaults.standard.set(Date(), forKey: kLastSubmitDate)
                    BurgerController.canSubmitAgain = false
                    self.tableView.reloadData()
                    print("Yay")
                }
            }
        }
    }
    
    @IBAction func uploadPhotoTapped() {
        setupImagePicker()
    }
    
    func resetBurgerInfo() {
        submitButton.setTitle("Submit", for: .normal)
        selectedIndexPaths = []
        NotificationCenter.default.post(Notification.init(name: Notification.Name(rawValue: "resetSelectedIndex")))
        self.tellUsAboutYourBurgerButton.isEnabled = true
        uploadButton.layer.borderWidth = 0.5
        uploadButton.setTitle("Submit A Photo", for: .normal)
        uploadButton.backgroundColor = UIColor.white
        self.imageView.image = nil
        self.nameYourBurgerTextField.text = ""
        self.userNameTextField.text = ""
        self.userEmailTextField.text = ""
        self.userFavLocationTextField.text = ""
        self.userPhoneNumberTextField.text = ""
        BurgerController.burger.userName = ""
        BurgerController.burger.burgerName = ""
        BurgerController.burger.userEmail = ""
        BurgerController.burger.userPhone = ""
        BurgerController.burger.userFavLocation = ""
        BurgerController.burger.cheese = []
        BurgerController.burger.sauces = []
        BurgerController.burger.veggies = []
        BurgerController.burger.size = []
        BurgerController.burger.goodies = []
    }
    
    //==========================================================================
    //  MARK: - Alerts
    //==========================================================================
    
    func presentSuccessfulSubmitAlert() {
        let alertController = UIAlertController(title: "Thank You for submitting to the 'Tonyburgers submit your burger challange'.", message: nil, preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: "Dismiss", style: .cancel, handler: nil)
        alertController.addAction(dismissAction)
        self.present(alertController, animated: true, completion: nil)
    }

    func presentAlert(and message: String) {
        let alertController = UIAlertController(title: message, message: nil, preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: "Dismiss", style: .cancel, handler: nil)
        alertController.addAction(dismissAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    //==========================================================================
    //  MARK: - Image Picker
    //==========================================================================
    
    func setupImagePicker() {
        self.resignFirstResponders()
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            alert.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (_) -> Void in
                imagePicker.sourceType = .photoLibrary
                self.present(imagePicker, animated: true, completion: nil)
            }))
        }
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (_) -> Void in
                imagePicker.sourceType = .camera
                self.present(imagePicker, animated: true, completion: nil)
            }))
        }
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        present(alert, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            delegate?.photoSelectViewControllerSelected(image: image)
            uploadButton.layer.borderWidth = 0
            uploadButton.setTitle("", for: .normal)
            uploadButton.backgroundColor = nil
            imageView.image = image
            self.image = image
            BurgerController.burger.image = image
        }
    }
    
    weak var delegate: PhotoSelectViewControllerDelegate?
    
}

protocol PhotoSelectViewControllerDelegate: class {
    
    func photoSelectViewControllerSelected(image: UIImage)
}

public enum Model : String {
    case simulator = "simulator/sandbox",
    iPod1          = "iPod 1",
    iPod2          = "iPod 2",
    iPod3          = "iPod 3",
    iPod4          = "iPod 4",
    iPod5          = "iPod 5",
    iPad2          = "iPad 2",
    iPad3          = "iPad 3",
    iPad4          = "iPad 4",
    iPhone4        = "iPhone 4",
    iPhone4S       = "iPhone 4S",
    iPhone5        = "iPhone 5",
    iPhone5S       = "iPhone 5S",
    iPhone5C       = "iPhone 5C",
    iPadMini1      = "iPad Mini 1",
    iPadMini2      = "iPad Mini 2",
    iPadMini3      = "iPad Mini 3",
    iPadAir1       = "iPad Air 1",
    iPadAir2       = "iPad Air 2",
    iPhone6        = "iPhone 6",
    iPhone6plus    = "iPhone 6 Plus",
    iPhone6S       = "iPhone 6S",
    iPhone6Splus   = "iPhone 6S Plus",
    iPhone7        = "iPhone 7",
    iPhone7plus    = "iPhone 7 Plus",
    unrecognized   = "?unrecognized?"
}

public extension UIDevice {
    public var type: Model {
        var systemInfo = utsname()
        uname(&systemInfo)
        let modelCode = withUnsafePointer(to: &systemInfo.machine) {
            $0.withMemoryRebound(to: CChar.self, capacity: 1) {
                ptr in String.init(validatingUTF8: ptr)
                
            }
        }
        var modelMap : [ String : Model ] = [
            "i386"      : .simulator,
            "x86_64"    : .simulator,
            "iPod1,1"   : .iPod1,
            "iPod2,1"   : .iPod2,
            "iPod3,1"   : .iPod3,
            "iPod4,1"   : .iPod4,
            "iPod5,1"   : .iPod5,
            "iPad2,1"   : .iPad2,
            "iPad2,2"   : .iPad2,
            "iPad2,3"   : .iPad2,
            "iPad2,4"   : .iPad2,
            "iPad2,5"   : .iPadMini1,
            "iPad2,6"   : .iPadMini1,
            "iPad2,7"   : .iPadMini1,
            "iPhone3,1" : .iPhone4,
            "iPhone3,2" : .iPhone4,
            "iPhone3,3" : .iPhone4,
            "iPhone4,1" : .iPhone4S,
            "iPhone5,1" : .iPhone5,
            "iPhone5,2" : .iPhone5,
            "iPhone5,3" : .iPhone5C,
            "iPhone5,4" : .iPhone5C,
            "iPad3,1"   : .iPad3,
            "iPad3,2"   : .iPad3,
            "iPad3,3"   : .iPad3,
            "iPad3,4"   : .iPad4,
            "iPad3,5"   : .iPad4,
            "iPad3,6"   : .iPad4,
            "iPhone6,1" : .iPhone5S,
            "iPhone6,2" : .iPhone5S,
            "iPad4,1"   : .iPadAir1,
            "iPad4,2"   : .iPadAir2,
            "iPad4,4"   : .iPadMini2,
            "iPad4,5"   : .iPadMini2,
            "iPad4,6"   : .iPadMini2,
            "iPad4,7"   : .iPadMini3,
            "iPad4,8"   : .iPadMini3,
            "iPad4,9"   : .iPadMini3,
            "iPhone7,1" : .iPhone6plus,
            "iPhone7,2" : .iPhone6,
            "iPhone8,1" : .iPhone6S,
            "iPhone8,2" : .iPhone6Splus,
            "iPhone9,1" : .iPhone7,
            "iPhone9,3" : .iPhone7,
            "iPhone9,4" : .iPhone7plus,
            "iPhone9,2" : .iPhone7plus
        ]
        
        if let model = modelMap[String.init(modelCode!)!] {
            return model
        }
        return Model.unrecognized
    }
}
