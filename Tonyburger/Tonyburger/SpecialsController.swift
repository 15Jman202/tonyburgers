//
//  SpecialsController.swift
//  Tonyburgers
//
//  Created by Justin Carver on 10/16/16.
//  Copyright © 2016 Justin Carver. All rights reserved.
//

import Foundation

let baseURL = NSURL(string: "https://tonyburgers-c7168.firebaseio.com/api/Specials")
let getterEndPoint = baseURL?.appendingPathExtension("json")

class SpecialController {

    static func getSpecials(completion: @escaping (_ specials: [Special]) -> Void) {

        guard let url = getterEndPoint else { return }

        NetworkController.performRequest(for: url, httpMethod: .get) { (data, error) in
            guard let data = data, error == nil else { print(error?.localizedDescription ?? ""); completion([]); return }

            guard let jsonDictionary = (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)) as? [String : [String: String]] else { completion([]); return }

            let specialsArray = jsonDictionary.flatMap { Special(dictionary: $0.value) }

            completion(specialsArray)
        }
    }
}
