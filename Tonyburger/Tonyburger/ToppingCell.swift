//
//  ToppingCell.swift
//  Tonyburgers
//
//  Created by Justin Carver on 11/17/16.
//  Copyright © 2016 Justin Carver. All rights reserved.
//

import UIKit

class ToppingCell: UITableViewCell {
    
    @IBOutlet var contactImageView: UIImageView!
    @IBOutlet var toppingLabel: UILabel!
    
    func updateCellWith(topping: String) {
        toppingLabel.text = topping
    }
    
    func updateCellWith(Image: UIImage) {
        contactImageView.image = Image
    }
}
