//
//  specialsTableViewController.swift
//  Tonyburgers
//
//  Created by Justin Carver on 10/16/16.
//  Copyright © 2016 Justin Carver. All rights reserved.
//

import UIKit

class specialsTableViewController: UITableViewController {
    
    static var specials: [Special] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SpecialController.getSpecials { (specials) -> Void in
            DispatchQueue.main.async {
            specialsTableViewController.specials = specials
            self.tableView.reloadData()
            }
        }
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let special = specialsTableViewController.specials[indexPath.row]
        if special.text.characters.count > 25 {
            return 70.0
        } else if special.text.characters.count > 75 {
            return 70.0
        } else {
            return 40.0
        }
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return specialsTableViewController.specials.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "specialsCell", for: indexPath)
        let special = specialsTableViewController.specials[indexPath.row]
        
        cell.textLabel?.text = special.text
        cell.detailTextLabel?.text = special.endDate.description
        
        return cell
    }
}
