//
//  Specials.swift
//  Tonyburgers
//
//  Created by Justin Carver on 10/16/16.
//  Copyright © 2016 Justin Carver. All rights reserved.
//

import Foundation

class Special {
    
    private let kText = "text"
    private let kEndDate = "endDate"
    
    var endDate: String
    var text: String
    
    init?(dictionary: [String: String]) {
        guard let text = dictionary[kText], let endDate = dictionary[kEndDate] else { return nil }
        
        self.text = text
        self.endDate = endDate
    }
}
