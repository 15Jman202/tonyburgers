//
//  BurgerController.swift
//  Tonyburgers
//
//  Created by Justin Carver on 11/16/16.
//  Copyright © 2016 Justin Carver. All rights reserved.
//

import Foundation

class BurgerController {
    
    static var burger: Burger = Burger()
    
    static var canSubmitAgain: Bool = true
    
    static let baseUrl = URL(string: "https://tonyburgers-c7168.firebaseio.com/api/SubmittedBurgers")
    static let uuid = UUID()
    static let endUrl = BurgerController.baseUrl?.appendingPathComponent(BurgerController.uuid.uuidString).appendingPathExtension("json")
    
    static func putBurgerInformation(burgerInfo: Burger, success: @escaping (_ success: Bool) -> Void) {
        guard let url = BurgerController.endUrl else { success(false); return }
        
        NetworkController.performRequest(for: url as URL, httpMethod: .put, body: burgerInfo.jsonData) { (data, error) in
            DispatchQueue.main.async {
                if error != nil {
                    print(error?.localizedDescription ?? "error")
                    success(false)
                    return
                } else {
                    success(true)
                    print("Put burger")
                    return
                }
            }
        }
    }
}
