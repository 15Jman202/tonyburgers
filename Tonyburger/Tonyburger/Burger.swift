//
//  Burger.swift
//  Tonyburgers
//
//  Created by Justin Carver on 11/15/16.
//  Copyright © 2016 Justin Carver. All rights reserved.
//

import Foundation
import UIKit

class Burger {
    
    private let kBurgerName = "Burger Name"
    private let kSize = "Burger Size"
    private let kCheese = "Cheese"
    private let kVeggies = "Veggies"
    private let kSauces = "Sauces"
    private let kGoodies = "Goodies"
    private let kImageUUID = "ImageUUID"
    private let kUserEmail = "User Email"
    private let kUserName = "User Name"
    private let kUserPhone = "User Phone Number"
    private let kUserFavLocation = "User Favorite Location"
    private let kSubmitDate = "Date Submitted"
    
    var burgerName: String
    var size: [String]
    var cheese: [String]
    var veggies: [String]
    var sauces: [String]
    var goodies: [String]
    var image: UIImage?
    var userEmail: String
    var userName: String
    var userPhone: String
    var userFavLocation: String
    var submitDate: Date?
    
    
    init(burgerName: String = "", size: [String] = [], cheese: [String] = [], veggies: [String] = [], sauces: [String] = [], goodies: [String] = [], image: UIImage? = nil, userEmail: String = "", userName: String =  "", userPhone: String = "", userFavLocation: String = "", submitDate: Date? = nil) {
        self.burgerName = burgerName
        self.size = size
        self.cheese = cheese
        self.veggies = veggies
        self.sauces = sauces
        self.goodies = goodies
        self.image = image
        self.userName = userName
        self.userEmail = userEmail
        self.userPhone = userPhone
        self.userFavLocation = userFavLocation
        self.submitDate = submitDate
    }
    
    var jsonData: Data? {
        return try? JSONSerialization.data(withJSONObject: dictionaryRep, options: .prettyPrinted)
    }
    
    var dictionaryRep: [String: Any] {
        return [kImageUUID: BurgerController.uuid.uuidString, kBurgerName: burgerName, kSize: size, kCheese: cheese, kVeggies: veggies, kSauces: sauces, kGoodies: goodies, kUserEmail: userEmail, kUserName: userName, kUserPhone: userPhone, kUserFavLocation: userFavLocation, kSubmitDate: submitDate?.description ?? Date().description]
    }
}
