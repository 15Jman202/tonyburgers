//
//  AppDelegate.swift
//  Tonyburger
//
//  Created by Justin Carver on 10/11/16.
//  Copyright © 2016 Justin Carver. All rights reserved.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        FIRApp.configure()
        SpecialController.getSpecials { (specials) -> Void in
            DispatchQueue.main.async {
                specialsTableViewController.specials = specials
            }
        }
        return true
    }
}
