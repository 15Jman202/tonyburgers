//
//  SaladViewController.swift
//  Tonyburgers
//
//  Created by Justin Carver on 10/13/16.
//  Copyright © 2016 Justin Carver. All rights reserved.
//

import UIKit

class SaladViewController: UIViewController, UIPopoverPresentationControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    @IBOutlet var selectionsButton: UIButton!
    @IBOutlet var saladSizeButton: UIButton!
    @IBOutlet var specialToppingButton: UIButton!
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        UIView.animate(withDuration: 0.5) {
            if segue.identifier == "toSaladSizePopover" {
                let sourceRect = self.saladSizeButton.imageView?.frame.insetBy(dx: 0, dy: 0)
                let popoverViewController = segue.destination as! PopoverViewController
                popoverViewController.popoverPresentationController?.sourceRect = sourceRect!
                popoverViewController.modalPresentationStyle = UIModalPresentationStyle.popover
                popoverViewController.popoverPresentationController?.delegate = self
            }
            if segue.identifier == "toSelectionsPopover" {
                let sourceRect = self.selectionsButton.imageView?.frame.insetBy(dx: 0, dy: 0)
                let popoverViewController = segue.destination as! PopoverViewController
                popoverViewController.popoverPresentationController?.sourceRect = sourceRect!
                popoverViewController.modalPresentationStyle = UIModalPresentationStyle.popover
                popoverViewController.popoverPresentationController?.delegate = self
            }
            if segue.identifier == "toSpecialToppingsPopover" {
                let sourceRect = self.specialToppingButton.imageView?.frame.insetBy(dx: 0, dy: 0)
                let popoverViewController = segue.destination as! PopoverViewController
                popoverViewController.popoverPresentationController?.sourceRect = sourceRect!
                popoverViewController.modalPresentationStyle = UIModalPresentationStyle.popover
                popoverViewController.popoverPresentationController?.delegate = self
            }
        }
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
}
