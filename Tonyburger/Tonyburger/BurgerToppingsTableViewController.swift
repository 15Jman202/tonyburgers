//
//  BurgerToppingsTableViewController.swift
//  Tonyburgers
//
//  Created by Justin Carver on 11/17/16.
//  Copyright © 2016 Justin Carver. All rights reserved.
//

import UIKit

class BurgerToppingsTableViewController: UITableViewController {
    
    let sizes = ["Small Tony", "Big-T", "Double"]
    let cheeses = ["American", "Chedder", "Swiss", "Pepper Jack"]
    let goodies = ["Soft Fried Egg", "Hard Fried Egg", "Mushrooms", "Bacon", "Onion Strings"]
    let sauces = ["Tony Sauce", "Ketchup", "Mustard", "Mayo", "Ranch", "Southern Ranch", "BBQ"]
    let veggies = ["Lettuce", "Tomato", "Raw Onions", "Grilled Onions", "Pickles", "Jalapenos"]
    
    var selectedIndexPaths: [IndexPath] = [] {
        didSet {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "selectedIndexPathsUpdated"), object: selectedIndexPaths)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(resetSelectedIndexPaths), name: NSNotification.Name("resetSelectedIndex"), object: nil)
    }
    
    //==========================================================================
    //  MARK: - Notifications
    //==========================================================================
    
    func resetSelectedIndexPaths() {
        self.selectedIndexPaths = []
    }
    
    func toppingCellTappedToDelete(topping: String) {
        if sizes.contains(topping) {
            guard BurgerController.burger.size.count != 0 else { return }
            let index = BurgerController.burger.size.index(of: topping)
            guard index != nil else { return }
            BurgerController.burger.size.remove(at: index!)
        } else if cheeses.contains(topping) {
            guard BurgerController.burger.cheese.count != 0 else { return }
            let index = BurgerController.burger.cheese.index(of: topping)
            guard index != nil else { return }
            BurgerController.burger.cheese.remove(at: index!)
        } else if goodies.contains(topping) {
            guard BurgerController.burger.goodies.count != 0 else { return }
            let index = BurgerController.burger.goodies.index(of: topping)
            guard index != nil else { return }
            BurgerController.burger.goodies.remove(at: index!)
        } else if sauces.contains(topping) {
            guard BurgerController.burger.sauces.count != 0 else { return }
            let index = BurgerController.burger.sauces.index(of: topping)
            guard index != nil else { return }
            BurgerController.burger.sauces.remove(at: index!)
        } else if veggies.contains(topping) {
            guard BurgerController.burger.veggies.count != 0 else { return }
            let index = BurgerController.burger.veggies.index(of: topping)
            guard index != nil else { return }
            BurgerController.burger.veggies.remove(at: index!)
        }
    }
    
    func toppingCellTappedToAdd(topping: String) {
        if sizes.contains(topping) {
            BurgerController.burger.size.append(topping)
        } else if cheeses.contains(topping) {
            BurgerController.burger.cheese.append(topping)
        } else if goodies.contains(topping) {
            BurgerController.burger.goodies.append(topping)
        } else if sauces.contains(topping) {
            BurgerController.burger.sauces.append(topping)
        } else if veggies.contains(topping) {
            BurgerController.burger.veggies.append(topping)
        }
    }

    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Size"
        } else if section == 1 {
            return "Cheese"
        } else if section == 2 {
            return "Goodies"
        } else if section == 3 {
            return "Sauce"
        } else {
            return "Veggies"
        }
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? ToppingCell, let topping = cell.toppingLabel.text else { return }

        // if selected index path already contains the index path remove the topping else add the topping
        
        if selectedIndexPaths.contains(indexPath) {
            toppingCellTappedToDelete(topping: topping)
            cell.updateCellWith(Image: #imageLiteral(resourceName: "Add Contact"))
            guard let indexOfIndexPath = selectedIndexPaths.index(of: indexPath) else { return }
            selectedIndexPaths.remove(at: indexOfIndexPath)
        } else {
            toppingCellTappedToAdd(topping: topping)
            cell.updateCellWith(Image: #imageLiteral(resourceName: "Remove Contact"))
            selectedIndexPaths.append(indexPath)
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 3
        } else if section == 1 {
            return 4
        } else if section == 2 {
            return 5
        } else if section == 3 {
            return 7
        } else {
            return 6
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let actualCell = cell as? ToppingCell else { return }
        if selectedIndexPaths.contains(indexPath) {
            actualCell.updateCellWith(Image: #imageLiteral(resourceName: "Remove Contact"))
        } else {
            actualCell.updateCellWith(Image: #imageLiteral(resourceName: "Add Contact"))
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "toppingCell", for: indexPath) as? ToppingCell else { return ToppingCell() }
        
        var topping = ""
        
        if indexPath.section == 0 {
            topping = sizes[indexPath.row]
        } else if indexPath.section == 1 {
            topping = cheeses[indexPath.row]
        } else if indexPath.section == 2 {
            topping = goodies[indexPath.row]
        } else if indexPath.section == 3 {
            topping = sauces[indexPath.row]
        } else {
            topping = veggies[indexPath.row]
        }
        
        cell.updateCellWith(topping: topping)
        
        return cell
    }
}
