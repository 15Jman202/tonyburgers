//
//  StartViewController.swift
//  Tonyburgers
//
//  Created by Justin Carver on 10/20/16.
//  Copyright © 2016 Justin Carver. All rights reserved.
//

import UIKit

let kLastSubmitDate = "lastSubmitDate"

class StartViewController: UIViewController {
    
    private let kHasOpenedBefore = "hasOpenedBefore"
    
    var hasOpenedBefore: Bool = false
    var lastSubmitDate: Date?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        MainScreenViewController.load()
        sloganLabel.alpha = 0
        hasOpenedBefore = UserDefaults.standard.bool(forKey: kHasOpenedBefore)
        loadLastSubmitDate()
        if hasOpenedBefore == true {
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "toMainScreen", sender: self)
            }
        } else {
            hasOpenedBefore = true
            saveHasOpenedBefore()
        }
        guard let lastSubmit = self.lastSubmitDate else { return }
        
        if lastSubmit.addingTimeInterval(1296000) >= Date.init() {
            BurgerController.canSubmitAgain = false
        } else {
            BurgerController.canSubmitAgain = true
        }
    }
    
    func loadLastSubmitDate() {
        if let date = UserDefaults.standard.object(forKey: kLastSubmitDate) as? Date {
            lastSubmitDate = date
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 2.54, delay: 1.0, options: .beginFromCurrentState, animations: {
            self.logoImageView.center.y = self.view.center.y - 100
            }, completion: { (_) in
            UIView.animate(withDuration: 0.5, delay: 0.5, options: .beginFromCurrentState, animations: {
                self.sloganLabel.alpha = 1.0
                }, completion: { (_) in
                    sleep(3)
                DispatchQueue.main.async {
                    self.performSegue(withIdentifier: "toMainScreen", sender: self)
                }
            })
        })
    }
    
    func saveHasOpenedBefore() {
        UserDefaults.standard.set(hasOpenedBefore, forKey: kHasOpenedBefore)
    }
    
    @IBOutlet var logoImageView: UIImageView!
    @IBOutlet var sloganLabel: UILabel!
}
