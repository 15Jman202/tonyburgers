//
//  MainScreenViewController.swift
//  Tonyburgers
//
//  Created by Justin Carver on 10/12/16.
//  Copyright © 2016 Justin Carver. All rights reserved.
//

import UIKit

class MainScreenViewController: UIViewController, UIPopoverPresentationControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        let imageview = UIImageView(image: #imageLiteral(resourceName: "Logo"))
        imageview.contentMode = .scaleAspectFit
        imageview.frame = CGRect(x: 0, y: 0, width: 50, height: 45)
        navigationItem.titleView = imageview
    }

    @IBOutlet var shakesButton: UIButton!
    @IBOutlet var friesButton: UIButton!
    @IBOutlet var justForKidsButton: UIButton!
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        UIView.animate(withDuration: 0.5) {
        if segue.identifier == "toFriesPopover" {
            let sourceRect = self.friesButton.imageView?.frame.insetBy(dx: 15, dy: 0)
            let popoverViewController = segue.destination as! PopoverViewController
            popoverViewController.popoverPresentationController?.sourceRect = sourceRect!
            popoverViewController.modalPresentationStyle = UIModalPresentationStyle.popover
            popoverViewController.popoverPresentationController?.delegate = self
        }
        if segue.identifier == "toShakePopover" {
            let sourceRect = self.shakesButton.imageView?.frame.insetBy(dx: -10, dy: 0)
            let popoverViewController = segue.destination as! PopoverViewController
            popoverViewController.popoverPresentationController?.sourceRect = sourceRect!
            popoverViewController.modalPresentationStyle = UIModalPresentationStyle.popover
            popoverViewController.popoverPresentationController?.delegate = self
        }
        if segue.identifier == "toKidsMealPopover" {
            let sourceRect = self.justForKidsButton.imageView?.frame.insetBy(dx: -11, dy: 0)
            let popoverViewController = segue.destination as! PopoverViewController
            popoverViewController.popoverPresentationController?.sourceRect = sourceRect!
            popoverViewController.modalPresentationStyle = UIModalPresentationStyle.popover
            popoverViewController.popoverPresentationController?.delegate = self
            }
        }
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
}
