//
//  TonyburgersUITests.swift
//  TonyburgersUITests
//
//  Created by Justin Carver on 10/13/16.
//  Copyright © 2016 Justin Carver. All rights reserved.
//

import XCTest

class TonyburgersUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testExample() {
            
    }
    
}
